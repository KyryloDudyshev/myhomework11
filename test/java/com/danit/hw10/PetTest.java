package myhomework11.test.java.com.danit.hw10;
import myhomework11.main.java.com.danit.hw11.Dog;
import myhomework11.main.java.com.danit.hw11.DomesticCat;
import myhomework11.main.java.com.danit.hw11.Fish;
import myhomework11.main.java.com.danit.hw11.Pet;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class PetTest {
    Pet pet = new Dog();
    Pet pet2 = new DomesticCat();

    @Test
    void testToString() {
        String actual = pet.toString();
        String expected = pet.getSpecies() + " nickname = " + pet.getNickname() + "," + " age = " + pet.getAge() + "," + " trickLevel = " + pet.getTrickLevel() + "," + " habits = " + pet.getHabits() + ", can fly " + pet.getSpecies().isCanFly() + ", has fur " + pet.getSpecies().isHasFur() + ", number of paw " + pet.getSpecies().getNumberOfPaw();
        assertEquals(expected,actual);
    }

    @Test
    void testHashcode(){
        assertTrue(pet.hashCode()!=pet2.hashCode());
    }

    @Test
    void testEqualsFalse(){
        assertFalse(pet2.equals(pet));
    }

    @Test
    void testEqualsTrue(){
        assertTrue(pet.equals(pet));
    }

    //contract Equals
    @Test
    void testSymmetry() {
        assertEquals(pet.equals(pet2),pet2.equals(pet));
    }

    @Test
    void testTransitivity() {
        Pet pet3 = new Fish();
        boolean[] test = {pet.equals(pet2),pet2.equals(pet3),pet.equals(pet3)};
        assertEquals(Arrays.toString(new boolean[]{false,false,false}),Arrays.toString(test));
    }

    @Test
    void testConsistency() {
        boolean firstInvoke = pet.equals(pet2);
        boolean secondInvoke = pet.equals(pet2);
        assertEquals(firstInvoke,secondInvoke);
    }

    @Test
    void testNull() {
        assertEquals(false,pet.equals(null));
    }
}
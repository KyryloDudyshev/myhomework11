package myhomework11.test.java.com.danit.hw10;
import myhomework11.main.java.com.danit.hw11.Family;
import myhomework11.main.java.com.danit.hw11.Human;
import myhomework11.main.java.com.danit.hw11.Man;
import myhomework11.main.java.com.danit.hw11.Woman;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class FamilyTest {
    Human john = new Man("John","Wick");
    Human karla = new Woman("Karla","Bruni");
    Family familyWick = new Family(karla, john);
    Family family = new Family(new Man(),new Woman());

    Human natali = new Woman("Natali","Portman");
    Woman liza = new Woman("Liza","Mineli");
    ArrayList<Human> children = new ArrayList<Human>(List.of(liza,natali));


    Human father = john;
    Human mother = karla;


    @Test
    void testToString() {
        familyWick.setChildren(children);
        String actual = familyWick.toString();
        String expected = "Family: mother: " + mother.getName() + " " + mother.getSurname() + ", " + "father: " + father.getName() + " " + father.getSurname() + ", children: " + children;
        assertEquals(expected,actual);
    }

    @Test
    void testDeleteChildCorrectName() {
        familyWick.setChildren(children);
        familyWick.deleteChild(natali);
        assertEquals(1,familyWick.getChildren().size());
    }

    @Test
    void testDeleteChildInCorrectName() {
        familyWick.setChildren(children);
        Human child = new Man("Alex","Smith");
        familyWick.deleteChild(child);
        assertEquals(2,familyWick.getChildren().size());
    }


    @Test
    void testdeleteChildInCorrectIndex() {
        familyWick.setChildren(children);
        familyWick.deleteChild(3);
        assertEquals(2,familyWick.getChildren().size());
    }

    @Test
    void testdeleteChildCorrectIndex() {
        familyWick.setChildren(children);
        familyWick.deleteChild(0);
        assertEquals(1,familyWick.getChildren().size());
    }

    @Test
    void testdeleteChildInCorrectIndexBoolean() {
        familyWick.setChildren(children);
        assertEquals(false,familyWick.deleteChild(3));
    }

    @Test
    void testdeleteChildCorrectIndexBoolean() {
        familyWick.setChildren(children);
        assertEquals(true,familyWick.deleteChild(1));
    }

    @Test
    void testAddChild() {
        familyWick.setChildren(children);
        Human child = new Man("Alex","Smith");
        familyWick.addChild(child);
        assertEquals(3,familyWick.getChildren().size());
    }

    @Test
    void testAddChildCheckObject() {
        familyWick.setChildren(children);
        Human child = new Man("Alex","Smith");
        familyWick.addChild(child);
        assertEquals(true,child.equals(familyWick.getChildren().get(2)));
    }

    @Test
    void testCountFamily() {
        familyWick.setChildren(children);
        assertEquals(4,familyWick.getChildren().size() + 2);
    }

    @Test
    void testHashcode(){
        assertTrue(family.hashCode()!=familyWick.hashCode());
    }

    @Test
    void testEqualsFalse(){
        assertFalse(family.equals(familyWick));
    }

    @Test
    void testEqualsTrue(){
        assertTrue(familyWick.equals(familyWick));
    }

    //contract Equals
    @Test
    void testSymmetry() {
        assertEquals(family.equals(familyWick),familyWick.equals(family));
    }

    @Test
    void testTransitivity() {
        Family family2 = new Family(new Man(),new Woman());
        boolean[] test = {familyWick.equals(family),family.equals(family2),familyWick.equals(family2)};
        assertEquals(Arrays.toString(new boolean[]{false,false,false}),Arrays.toString(test));
    }

    @Test
    void testConsistency() {
        boolean firstInvoke = familyWick.equals(family);
        boolean secondInvoke = familyWick.equals(family);
        assertEquals(firstInvoke,secondInvoke);
    }

    @Test
    void testNull() {
        assertEquals(false,familyWick.equals(null));
    }




}
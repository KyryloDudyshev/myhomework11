package myhomework11.test.java.com.danit.hw10;
import myhomework11.main.java.com.danit.hw11.DayOfWeek;
import myhomework11.main.java.com.danit.hw11.Family;
import myhomework11.main.java.com.danit.hw11.Man;
import myhomework11.main.java.com.danit.hw11.Woman;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;

class WomanTest {
    Woman woman = new Woman("Alexa","Paterson",300,new HashMap<DayOfWeek,String>());
    Man man = new Man("Joshua","Slow",198,new HashMap<DayOfWeek,String>());
    Family slow = new Family(woman,man);




    @Test
    void bornChildCorrectSurname() {
        woman.setFamily(slow);
        man.setFamily(slow);
        assertEquals("Slow",woman.bornChild().getSurname());
    }

    @Test
    void bornChildCorrectIq() {
        woman.setFamily(slow);
        man.setFamily(slow);
        assertEquals(249,woman.bornChild().getIq());
    }
}
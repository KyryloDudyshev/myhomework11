package myhomework11.main.java.com.danit.hw11;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Stream;

public class CollectionFamilyDao implements FamilyDao {
    private List<Family> families;
    SimpleDateFormat format = new SimpleDateFormat("yyyy");

    public CollectionFamilyDao() {
    }

    public CollectionFamilyDao(List<Family> families) {
        this.families = families;
    }

    public void setFamilies(List<Family> families) {
        this.families = families;
    }

    @Override
    public List getAllFamilies() {
        return families;
    }


    public void displayAllFamilies() {
        Stream.of(families).forEach(System.out::println);
    }


    public List getFamiliesBiggerThan(int quantity) {
        List<Family> listBiggerFamily = new ArrayList<>();
        for (Family el :
                families) {
            if (el.countFamily() > quantity) {
                listBiggerFamily.add(el);
            }

            }
        System.out.println(listBiggerFamily);
        return listBiggerFamily;
    }

    public List getFamiliesLessThan(int quantity) {
        List<Family> listBiggerFamily = new ArrayList<>();
        for (Family el :
                families) {
            if (el.countFamily() < quantity) {
                listBiggerFamily.add(el);
            }

        }
        System.out.println(listBiggerFamily);
        return listBiggerFamily;
    }


    public int countFamiliesWithMemberNumber(int quantity) {
        int index = 0;
        for (Family el :
                families) {
            if (el.countFamily() == quantity) {
                index++;
            }

        }
        System.out.println(index);
        return index;
    }

    public void createNewFamily(Human mother, Human father) {
        Family family = new Family(mother,father);
        saveFamily(family);
    }
    @Override
    public Family getFamilyByIndex(int index) {
        if (getAllFamilies().isEmpty()) {
            return null;
        } else if (index >= 0 && index < getAllFamilies().size()) {
            return families.get(index);
        } else {
            return null;
        }
    }
    @Override
    public boolean deleteFamily(int index) {
        if (getAllFamilies().isEmpty()) {
            return false;
        }
        if (index >= 0 && index < getAllFamilies().size()) {
            for (int i = 0; i < getAllFamilies().size(); i++) {
                if (i == index) {
                    getAllFamilies().remove(i);
                }
            }
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean deleteFamily(Family family) {
        if (getAllFamilies().isEmpty()) {
            return false;
        }
        for (int i = 0; i < getAllFamilies().size(); i++) {
            if (getAllFamilies().get(i).equals(family)) {
                getAllFamilies().remove(family);
                return true;
            }
        }
        return false;
    }

    @Override
    public void saveFamily(Family family) {
        for (int i = 0; i < getAllFamilies().size(); i++) {
            if (getAllFamilies().get(i).equals(family)) {
                getAllFamilies().remove(i);
                break;
            }
        }
        getAllFamilies().add(family);
    }

    public Family bornChild(Family family, String manName, String womanName) {
        Random random = new Random();
        if (random.nextInt(2) == 0) {
            Woman girl = new Woman();
            girl.setName(womanName);
            girl.setFamily(family.getFather().getFamily());
            girl.setSurname(family.getFather().getSurname());
            girl.setIq((family.getFather().getIq() + family.getMother().getIq()) / 2);
            family.addChild(girl);
        } else {
            Man boy = new Man();
            boy.setName(manName);
            boy.setFamily(family.getFather().getFamily());
            boy.setSurname(family.getFather().getSurname());
            boy.setIq((family.getFather().getIq() + family.getMother().getIq()) / 2);
            family.addChild(boy);
        }
        saveFamily(family);
        return family;
    }


    public Family adoptChild(Family family, Human child) {
        family.addChild(child);
        saveFamily(family);
        return family;
    }

    public void deleteAllChildrenOlderThen(int year) throws ParseException {
        String yearStr = Integer.toString(year);
        Date year2 = format.parse(yearStr);
        for (int i = 0; i < families.size(); i++) {
            for (int j = 0; j < families.get(i).getChildren().size(); j++) {
                if (families.get(i).getChildren().get(j).getBirthdayDateFormat().before(year2)) {
                    families.get(i).deleteChild(j);
                    j--;
                }
            }
        }
    }

    public int count() {
        return getAllFamilies().size();
    }

    public HashSet<Pet> getPets (int index) {
        return families.get(index).getPetsSet();
    }


    public void addPet(int indexOfFamily, Pet pet) {
        HashSet<Pet> petHashSet = new HashSet<>();
        if (families.get(indexOfFamily).getPetsSet() == null){
            families.get(indexOfFamily).setPetsSet(petHashSet);
        }
        families.get(indexOfFamily).getPetsSet().add(pet);
    }


}


package myhomework11.main.java.com.danit.hw11;

import java.util.HashSet;

public abstract class Pet {
    private final Species species = Species.UNKNOWN;
    private String nickname;
    private int age;
    private int trickLevel;
    private HashSet<String> habits;

    public Pet(String nickname, int age, int trickLevel, HashSet habits) {
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    public Pet(String nickname) {
        this.nickname = nickname;
    }

    public Pet() {
    }

    public Species getSpecies() {
        return species;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public void setTrickLevel(int trickLevel) {
        if (trickLevel >= 0 & trickLevel <= 100) {
            this.trickLevel = trickLevel;
        } else {
            System.out.println("incorrect level of trick");
        }
    }

    public HashSet getHabits() {
        return habits;
    }

    public void setHabits(HashSet habits) {
        this.habits = habits;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    public String toString() {
        return this.getSpecies() + " nickname = " + this.getNickname() + "," + " age = " + this.getAge() + "," + " trickLevel = " + this.getTrickLevel() + "," + " habits = " + this.getHabits() + ", can fly " + this.getSpecies().isCanFly() + ", has fur " + this.getSpecies().isHasFur() + ", number of paw " + this.getSpecies().getNumberOfPaw();
    }

    @Override
    protected void finalize(){
        System.out.println("Object Pet deleted");
    }

    public void eat() {
        System.out.println("Я кушаю!");
    }

    public abstract void respond();

    public String prettyFormat() {
        return toString();
    }




}

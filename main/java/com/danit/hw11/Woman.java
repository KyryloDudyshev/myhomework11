package myhomework11.main.java.com.danit.hw11;

import java.text.ParseException;
import java.util.HashMap;
import java.util.Random;

public final class Woman extends Human implements HumanCreator {

    String[] girlsNames = {"Liza","Joana","Monica","Jessica"};
    String[] boysNames = {"Alex","Jason","Shon","Peter"};

    public Woman(String name, String surname) {
        super(name, surname);
    }

    public Woman(String name, String surname, int iq, HashMap<DayOfWeek,String> schedule) {
        super(name, surname, iq, schedule);
    }

    public Woman() {
    }

    public Woman(String name, String surname, String date, int iq) throws ParseException {
        super(name, surname, date, iq);
    }
    public void makeup() {
        System.out.println("I need to makeup");
    }

    @Override
    public void greetPet() {
        System.out.println("Привет, " + super.getFamily().getPet().getNickname() + "!");
    }

    @Override
    public Human bornChild() {
        Random random = new Random();
        if (random.nextInt(2) == 0) {
            Woman girl = new Woman();
            int random2 = random.nextInt(4);
            girl.setName(girlsNames[random2]);
            girl.setFamily(super.getFamily());
            girl.setSurname(super.getFamily().getFather().getSurname());
            girl.setIq((super.getFamily().getFather().getIq() + super.getFamily().getMother().getIq()) / 2);
            return girl;
        } else {
            Man boy = new Man();
            int random3 = random.nextInt(4);
            boy.setName(boysNames[random3]);
            boy.setFamily(super.getFamily());
            boy.setSurname(super.getFamily().getFather().getSurname());
            boy.setIq((super.getFamily().getFather().getIq() + super.getFamily().getMother().getIq()) / 2);
            return boy;
        }
    }

    @Override
    public String toString() {
        return "Woman {name = " + this.getName() + ", " + "surname = " + this.getSurname() + ", " + "birthday = " + this.birthday() + ", " + "iq = " + this.getIq() + ", " + "schedule =" + super.getSchedule() + "}";
    }
}

package myhomework11.main.java.com.danit.hw11;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class FamilyService {
    private CollectionFamilyDao collectionFamilyDao;

    public FamilyService(CollectionFamilyDao collectionFamilyDao) {
        this.collectionFamilyDao = collectionFamilyDao;
    }

    public CollectionFamilyDao getCollectionFamilyDao() {
        return collectionFamilyDao;
    }

    public void setCollectionFamilyDao(CollectionFamilyDao collectionFamilyDao) {
        this.collectionFamilyDao = collectionFamilyDao;
    }

    SimpleDateFormat format = new SimpleDateFormat("yyyy");

    public List getAllFamilies() {
        return collectionFamilyDao.getAllFamilies();
    }


    public void displayAllFamilies() {
        List<Family> familyList = getAllFamilies();
        for (Family family :
                familyList) {
            System.out.println(family.prettyFormat());
        }
    }


    public List getFamiliesBiggerThan(int quantity) {
        List<Family> familyList = collectionFamilyDao.getAllFamilies();
        System.out.println(familyList.stream().filter(family -> family.countFamily() > quantity).collect(Collectors.toList()));
        return familyList.stream().filter(family -> family.countFamily() > quantity).collect(Collectors.toList());
    }

    public List getFamiliesLessThan(int quantity) {
        List<Family> familyList = collectionFamilyDao.getAllFamilies();
        System.out.println(familyList.stream().filter(family -> family.countFamily() < quantity).collect(Collectors.toList()));
        return familyList.stream().filter(family -> family.countFamily() < quantity).collect(Collectors.toList());
    }


    public long countFamiliesWithMemberNumber(int quantity) {
        List<Family> familyList = collectionFamilyDao.getAllFamilies();
        System.out.println(familyList.stream().filter(family -> family.countFamily() == quantity).count());
        return familyList.stream().filter(family -> family.countFamily() == quantity).count();
    }

    public void createNewFamily(Human mother, Human father) {
        Family family = new Family(mother,father);
        collectionFamilyDao.saveFamily(family);
    }
    public Family getFamilyByIndex(int index) {
        return collectionFamilyDao.getFamilyByIndex(index);
    }

    public boolean deleteFamily(int index) {
        return collectionFamilyDao.deleteFamily(index);
    }

    public boolean deleteFamily(Family family) {
        return collectionFamilyDao.deleteFamily(family);
    }


    public void saveFamily(Family family) {
        collectionFamilyDao.saveFamily(family);
    }

    public Family bornChild(Family family, String manName, String womanName) {
        Random random = new Random();
        if (random.nextInt(2) == 0) {
            Woman girl = new Woman();
            girl.setName(womanName);
            girl.setFamily(family.getFather().getFamily());
            girl.setSurname(family.getFather().getSurname());
            girl.setIq((family.getFather().getIq() + family.getMother().getIq()) / 2);
            family.addChild(girl);
            girl.setBirthdayDateFormat(new Date());
        } else {
            Man boy = new Man();
            boy.setName(manName);
            boy.setFamily(family.getFather().getFamily());
            boy.setSurname(family.getFather().getSurname());
            boy.setIq((family.getFather().getIq() + family.getMother().getIq()) / 2);
            family.addChild(boy);
            boy.setBirthdayDateFormat(new Date());
        }
        saveFamily(family);
        return family;
    }


    public Family adoptChild(Family family, Human child) {
        family.addChild(child);
        saveFamily(family);
        return family;
    }

    public void deleteAllChildrenOlderThen(int year) throws ParseException {
        String yearStr = Integer.toString(year);
        Date year2 = format.parse(yearStr);
        for (int i = 0; i < collectionFamilyDao.getAllFamilies().size(); i++) {
            for (int j = 0; j < collectionFamilyDao.getFamilyByIndex(i).getChildren().size(); j++) {
                if (collectionFamilyDao.getFamilyByIndex(i).getChildren().get(j).getBirthdayDateFormat().before(year2)) {
                    collectionFamilyDao.getFamilyByIndex(i).deleteChild(j);
                    j--;
                }
            }
        }
    }

    public int count() {
        return collectionFamilyDao.getAllFamilies().size();
    }

    public HashSet<Pet> getPets (int index) {
        return collectionFamilyDao.getFamilyByIndex(index).getPetsSet();
    }


    public void addPet(int indexOfFamily, Pet pet) {
        HashSet<Pet> petHashSet = new HashSet<>();
        if (collectionFamilyDao.getFamilyByIndex(indexOfFamily).getPetsSet() == null){
            collectionFamilyDao.getFamilyByIndex(indexOfFamily).setPetsSet(petHashSet);
        }
        collectionFamilyDao.getFamilyByIndex(indexOfFamily).getPetsSet().add(pet);
    }








}

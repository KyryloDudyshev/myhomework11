package myhomework11.main.java.com.danit.hw11;

public enum Species {
    DOG, DOMESTICCAT, FISH, ROBOCAT, UNKNOWN;

    private boolean canFly;
    private int numberOfPaw;
    private boolean hasFur;

    Species(boolean canFly, int numberOfPaw, boolean hasFur) {
        this.canFly = canFly;
        this.numberOfPaw = numberOfPaw;
        this.hasFur = hasFur;
    }

    Species() {

    }

    public boolean isCanFly() {
        return canFly;
    }

    public void setCanFly(boolean canFly) {
        this.canFly = canFly;
    }

    public int getNumberOfPaw() {
        return numberOfPaw;
    }

    public void setNumberOfPaw(int numberOfPaw) {
        this.numberOfPaw = numberOfPaw;
    }

    public boolean isHasFur() {
        return hasFur;
    }

    public void setHasFur(boolean hasFur) {
        this.hasFur = hasFur;
    }
}

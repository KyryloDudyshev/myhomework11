package myhomework11.main.java.com.danit.hw11;

import java.text.ParseException;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;

public class FamilyController {
    private FamilyService familyService;

    public FamilyController(FamilyService familyService) {
        this.familyService = familyService;
    }

    public FamilyController() {
    }
    public void start() throws ParseException {
        while (true) {
            System.out.println("Menu");
            System.out.println("1. Заполнить тестовыми данными ");
            System.out.println("2. Отобразить весь список семей");
            System.out.println("3. Отобразить список семей, где количество людей больше заданного");
            System.out.println("4. Отобразить список семей, где количество людей меньше заданного");
            System.out.println("5. Подсчитать количество семей, где количество членов равно");
            System.out.println("6. Создать новую семью");
            System.out.println("7. Удалить семью по индексу семьи в общем списке");
            System.out.println("8. Редактировать семью по индексу семьи в общем списке ");
            System.out.println("9. Удалить всех детей старше возраста ");
            System.out.println("10. Exit");


            System.out.println("Сделайте свой выбор: ");
            Scanner scanner = new Scanner(System.in);
            if (!scanner.hasNextInt()) {
                System.out.println("Не верный ввод. Введите число от 1 до 10");
                continue;
            }
            int menuItem = scanner.nextInt();
            while (menuItem < 1 || menuItem > 10) {
                System.out.println("Не верный ввод");
                menuItem = scanner.nextInt();
            }
            switch (menuItem) {
                case 1:
                    familyService.getCollectionFamilyDao().setFamilies(AutoFamilyCreator.AutoFamilyCreator());
                    System.out.println("База заполнена автоматически");
                    break;
                case 2:
                    displayAllFamilies();
                    break;
                case 3:
                    System.out.println("Введите количество людей: ");
                    scanner = new Scanner(System.in);
                    int number = scanner.nextInt();
                    List<Family> families = getFamiliesBiggerThan(number);
                    FamilyService familyService1 = new FamilyService(new CollectionFamilyDao(families));
                    familyService1.displayAllFamilies();
                    break;
                case 4:
                    System.out.println("Введите количество людей: ");
                    scanner = new Scanner(System.in);
                    int number2 = scanner.nextInt();
                    List<Family> families2 = getFamiliesLessThan(number2);
                    FamilyService familyService2 = new FamilyService(new CollectionFamilyDao(families2));
                    familyService2.displayAllFamilies();
                    break;
                case 5:
                    System.out.println("Введите количество людей: ");
                    scanner = new Scanner(System.in);
                    int number3 = scanner.nextInt();
                    countFamiliesWithMemberNumber(number3);
                    break;
                case 6:
                    System.out.println("Введите имя матери: ");
                    scanner = new Scanner(System.in);
                    String mothersName = scanner.nextLine();
                    System.out.println("Введите фамилию матери: ");
                    scanner = new Scanner(System.in);
                    String mothersSurname = scanner.nextLine();
                    System.out.println("Введите дату рождения матери: ");
                    scanner = new Scanner(System.in);
                    String date = scanner.nextLine();
                    System.out.println("Введите IQ матери: ");
                    scanner = new Scanner(System.in);
                    int mothersIq = scanner.nextInt();
                    System.out.println("Введите имя отца: ");
                    scanner = new Scanner(System.in);
                    String fathersName = scanner.nextLine();
                    System.out.println("Введите фамилию отца: ");
                    scanner = new Scanner(System.in);
                    String fathersSurname = scanner.nextLine();
                    System.out.println("Введите дату рождения отца: ");
                    scanner = new Scanner(System.in);
                    String dateFather = scanner.nextLine();
                    System.out.println("Введите IQ отца: ");
                    scanner = new Scanner(System.in);
                    int fathersIq = scanner.nextInt();
                    createNewFamily(new Woman(mothersName,mothersSurname,date,mothersIq),new Man(fathersName,fathersSurname,dateFather,fathersIq));
                    break;
                case 7:
                    System.out.println("Введите порядковый номер семьи: ");
                    scanner = new Scanner(System.in);
                    int familyIndex = scanner.nextInt();
                    familyService.deleteFamily(familyIndex);
                    break;
                case 8:
                    System.out.println("1. Родить ребёнка. ");
                    System.out.println("2. Усыновить ребенка. ");
                    System.out.println("3. Вернуться в главное меню.");
                    scanner = new Scanner(System.in);
                    int menuItem2 = scanner.nextInt();
                    switch (menuItem2) {
                        case 1:
                            System.out.println("Введите порядковый номер семьи");
                            scanner = new Scanner(System.in);
                            int indexOfFamily = scanner.nextInt();
                            System.out.println("Введите имя девочки");
                            scanner = new Scanner(System.in);
                            String womanName = scanner.nextLine();
                            System.out.println("Введите имя мальчика");
                            scanner = new Scanner(System.in);
                            String manName = scanner.nextLine();
                            bornChild((Family) getAllFamilies().get(indexOfFamily), manName, womanName);
                            break;
                        case 2:
                            System.out.println("Введите порядковый номер семьи");
                            scanner = new Scanner(System.in);
                            int indexOfFamily2 = scanner.nextInt();
                            System.out.println("Введите имя ребёнка");
                            scanner = new Scanner(System.in);
                            String nameAdoptedChild = scanner.nextLine();
                            System.out.println("Введите фамилию ребёнка");
                            scanner = new Scanner(System.in);
                            String surnameAdoptedChild = scanner.nextLine();
                            System.out.println("Введите дату рождения ребёнка");
                            scanner = new Scanner(System.in);
                            String dateAdoptedChild = scanner.nextLine();
                            System.out.println("Введите IQ ребёнка");
                            scanner = new Scanner(System.in);
                            int iqAdoptedChild = scanner.nextInt();
                            adoptChild((Family) getAllFamilies().get(indexOfFamily2), new Human(nameAdoptedChild, surnameAdoptedChild, dateAdoptedChild, iqAdoptedChild));
                            break;
                        case 3:
                    }
                case 9:
                    System.out.println("Введите возраст ребёнка");
                    scanner = new Scanner(System.in);
                    int ageOfChild = scanner.nextInt();
                    deleteAllChildrenOlderThen(ageOfChild);
                    break;
                case 10:
                    System.exit(0);
            }


        }

    }

    public void setFamilyService(FamilyService familyService) {
        this.familyService = familyService;
    }

    public List getAllFamilies() {
        return familyService.getAllFamilies();
    }

    public void displayAllFamilies() {
        familyService.displayAllFamilies();
    }

    public List getFamiliesBiggerThan(int quantity) {
        return familyService.getFamiliesBiggerThan(quantity);
    }

    public List getFamiliesLessThan(int quantity) {
        return familyService.getFamiliesLessThan(quantity);
    }

    public long countFamiliesWithMemberNumber(int quantity) {
        return familyService.countFamiliesWithMemberNumber(quantity);
    }

    public void createNewFamily(Human mother, Human father) {
        familyService.createNewFamily(mother,father);
    }

    public Family bornChild(Family family, String manName, String womanName) {
        return familyService.bornChild(family,manName,womanName);
    }

    public Family adoptChild(Family family, Human child) {
        return familyService.adoptChild(family, child);
    }

    public void deleteAllChildrenOlderThen(int year) throws ParseException {
        familyService.deleteAllChildrenOlderThen(year);
    }

    public int count() {
        return familyService.count();
    }

    public HashSet<Pet> getPets (int index) {
        return familyService.getCollectionFamilyDao().getFamilyByIndex(index).getPetsSet();
    }

    public void addPet(int indexOfFamily, Pet pet) {
        familyService.getCollectionFamilyDao().addPet(indexOfFamily,pet);
    }
}

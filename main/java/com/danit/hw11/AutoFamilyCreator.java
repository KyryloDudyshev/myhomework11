package myhomework11.main.java.com.danit.hw11;

import java.text.ParseException;
import java.util.*;

public class AutoFamilyCreator {

    public static List<Family> AutoFamilyCreator() throws ParseException {
        List<Family> families = new ArrayList<>();
        Man john = new Man("John","Wick", "03/12/2010",140);
        Woman karla = new Woman("Karla","Bruni","03/03/1686",89);
        Dog dog = new Dog("Alfa",3,95, new HashSet<String>(Set.of("play","go for a walk")));
        Woman natali = new Woman("Natali","Portman","03/03/2008",89);
        Woman liza = new Woman("Liza","Mineli","03/03/2003",89);
        Family familyWick = new Family(karla, john);
        familyWick.addChild(liza);
        familyWick.addChild(natali);
        HashSet<Pet> petsWick = new HashSet<>();
        petsWick.add(dog);
        familyWick.setPetsSet(petsWick);


        Man arnold = new Man("Arnold","Schwarzenegger","03/03/1686",89);
        Woman monica = new Woman("Monica","Belucci","03/03/1686",89);
        DomesticCat cat = new DomesticCat("Olaf",4,50, new HashSet<String>(Set.of("sleep","eat")));
        Woman michel = new Woman("Michel","Schwarzeneger","03/03/2007",89);
        RoboCat roboCat = new RoboCat("Martin",2,100,new HashSet<String>(Set.of("sleep","eat")));
        HashSet<Pet> petsArnold = new HashSet<>();
        petsArnold.add(cat);
        petsArnold.add(roboCat);
        Family familyArnold = new Family(monica, arnold);
        familyArnold.setPetsSet(petsArnold);
        familyArnold.addChild(michel);
        familyArnold.addChild(monica);
        familyArnold.setPet(cat);
        familyArnold.setPet(roboCat);

        families.add(familyArnold);
        families.add(familyWick);
        return families;

    }
}

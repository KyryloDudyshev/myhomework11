package myhomework11.main.java.com.danit.hw11;

import java.text.ParseException;
import java.util.HashMap;

public final class Man extends Human {

    public Man(String name, String surname) {
        super(name, surname);
    }

    public Man(String name, String surname, int iq, HashMap<DayOfWeek,String> schedule) {
        super(name, surname, iq, schedule);
    }

    public Man(String name, String surname, String date, int iq) throws ParseException {
        super(name, surname, date, iq);
    }

    public Man() {
    }

    public void repairCar() {
        System.out.println("I need to repair my car");
    }

    @Override
    public void greetPet() {
            System.out.println("Привет, " + super.getFamily().getPet().getNickname() + "!");
    }

    @Override
    public String toString() {
        return "Man {name = " + this.getName() + ", " + "surname = " + this.getSurname() + ", " + "birthday = " + this.birthday() + ", " + "iq = " + this.getIq() + ", " + "schedule =" + super.getSchedule() + "}";
    }

}
